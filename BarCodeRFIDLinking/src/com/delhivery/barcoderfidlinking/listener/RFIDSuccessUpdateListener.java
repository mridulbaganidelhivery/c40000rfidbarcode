package com.delhivery.barcoderfidlinking.listener;

/**
 * Callback return to main activity after seal is written on RFID Tag.
 * @author Mridul Bagani
 *
 */
public interface RFIDSuccessUpdateListener extends BaseListener{

	void onRFIDSuccessUpdate(String updatedValue);
}
