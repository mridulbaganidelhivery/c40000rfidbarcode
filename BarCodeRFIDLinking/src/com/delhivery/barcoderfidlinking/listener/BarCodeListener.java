package com.delhivery.barcoderfidlinking.listener;

/**
 * bar code listener
 * @author Mridul Bagani
 *
 */
public interface BarCodeListener extends BaseListener{
	
	void onBarCodeResponse(String result);

}
