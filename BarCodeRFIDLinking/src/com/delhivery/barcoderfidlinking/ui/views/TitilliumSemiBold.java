package com.delhivery.barcoderfidlinking.ui.views;

import com.delhivery.barcoderfidlinking.utils.UtilsDelhivery;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;


public class TitilliumSemiBold extends TextView {
	public TitilliumSemiBold(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setTypeface(UtilsDelhivery.getTypeface(context, "TitilliumWeb-SemiBold"));
	}

	public TitilliumSemiBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setTypeface(UtilsDelhivery.getTypeface(context, "TitilliumWeb-SemiBold"));
	}

	public TitilliumSemiBold(Context context) {
		super(context);
		this.setTypeface(UtilsDelhivery.getTypeface(context, "TitilliumWeb-SemiBold"));
	}
}
