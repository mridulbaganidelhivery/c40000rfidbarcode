package com.delhivery.barcoderfidlinking.ui;

import com.crittercism.app.Crittercism;
import com.delhivery.barcoderfidlinking.utils.AppConfig;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity{

	TextView btnWrite,btnErase,btnSearch;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Crittercism
		.initialize(getApplicationContext(), AppConfig.CrittercismId);
		btnWrite = (TextView)findViewById(R.id.btnWriteRFID);
		btnErase = (TextView)findViewById(R.id.btnEraseRFID);
		btnSearch = (TextView)findViewById(R.id.btnSearchRFID);
		btnWrite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent iWrite = new Intent(MainActivity.this,WriteandScanActivity.class);
				startActivity(iWrite);
			}
		});

		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent iWrite = new Intent(MainActivity.this,SearchActivity.class);
				startActivity(iWrite);
			}
		});

		btnErase.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent iErase = new Intent(MainActivity.this,EraseActivity.class);
				startActivity(iErase);
			}
		});
	}

}
