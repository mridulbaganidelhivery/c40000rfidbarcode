package com.delhivery.barcoderfidlinking.ui;

import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.delhivery.barcoderfidlinking.listener.BarCodeListener;
import com.delhivery.barcoderfidlinking.listener.RFIDSuccessUpdateListener;
import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.Tracer;
import com.delhivery.barcoderfidlinking.utils.UtilsDelhivery;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.RFIDWithUHF.BankEnum;
import com.zebra.adc.decoder.Barcode2DWithSoft;

public class BaseActivity extends Activity {

	private static String TAG = AppConfig.BASE_TAG + BaseActivity.class.getSimpleName();
	private Barcode2DWithSoft mReader;
	private static RFIDWithUHF mWriteRFID;
	private Boolean readerThreadStop = true;
	private static Boolean readerState = true, writerState = true;
	private BarCodeListener bListener;
	private static RFIDSuccessUpdateListener rfidSuccessListener;
	private WriterHandler mWriteHandler;
	private final static int RFID_TAG_SIZE = 24;
	private final static String INVALID_STRING = "Invalid String";
	private static final String RFID_WRITER_ERROR =  "RFID Writer not working.Scan again!";
	private static String currentSealWriteValue;
	public static final String RFID_TAG_WRITE_ERROR = "RFID Tag writing failure.Try again!";
	private static final String BARCODE_READER_FAIL = "Bar Code Reader not working.Scan again!";
	private static boolean run = true;
	private static CountDownTimer cTimer;
	private MediaPlayer mediaPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Tracer.debug(TAG, "[onCreate] _ " + "");
		mWriteHandler = new WriterHandler(getActivityContext());
		instantiateReader();
		instantiateRFIDWriter();
	}

	/**
	 * Initializes media player with beep sound
	 */
	protected void initRFIDBeepSound() {
		Tracer.debug(TAG, "[initRFIDBeepSound] _ " + "");
		if (mediaPlayer == null) {
			mediaPlayer = new MediaPlayer();
			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.rfid_success);
			try {
				if (mediaPlayer != null) {
					mediaPlayer.setDataSource(file.getFileDescriptor(),
							file.getStartOffset(), file.getLength());
					file.close();

					mediaPlayer.prepare();
				}
			} catch (IOException e) {
				mediaPlayer = null;
			}
		}
	}

	/**
	 * Plays a beep sound representing that an action started.
	 */
	protected void playRFIDBeepSound() {
		Tracer.debug(TAG, "[playRFIDBeepSound] _ " + "");
		if (mediaPlayer != null) {
			mediaPlayer.start();
		}

	}
	/**
	 * init writer RFID.
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	public class InitTask extends AsyncTask<String, Integer, Boolean> {
		ProgressDialog mypDialog;

		@Override
		protected Boolean doInBackground(String... params) {
			Tracer.debug(TAG, "[doInBackground] _ " + mWriteRFID.init());
			return mWriteRFID.init();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Tracer.debug(TAG, "[onPostExecute] _ " + "");
			mypDialog.cancel();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Tracer.debug(TAG, "[onPreExecute] _ " + "");
			mypDialog = new ProgressDialog(getActivityContext());
			mypDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mypDialog.setMessage("Starting RFID Writer...");
			mypDialog.setCanceledOnTouchOutside(false);
			mypDialog.show();
		}

	}


	/**
	 * instantiate bar code Reader
	 */
	private Boolean instantiateReader() {
		Tracer.debug(TAG, "[instantiateReader] _ " + "");
		try {
			Tracer.debug(TAG, "[instantiateReader] _ " + "try");
			mReader = Barcode2DWithSoft.getInstance();
			mReader.open(getActivityContext());
			readerState = true;
			return true;
		} catch (Exception ex) {
			Tracer.debug(TAG, "[instantiateReader] _ " + "catch" + ex.toString());
			Toast.makeText(this,BARCODE_READER_FAIL , Toast.LENGTH_SHORT).show();
			readerState = false;
			return false;
		}
	}

	/**
	 * instantiate rfid writer
	 * 
	 * @return
	 */
	private Boolean instantiateRFIDWriter() {
		Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "");
		try {
			mWriteRFID = RFIDWithUHF.getInstance();
			writerState = true;
			if (mWriteRFID != null && writerState)
				new InitTask().execute();
			return true;

		} catch (Exception ex) {
			Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "catch" + ex.toString());
			Toast.makeText(this,RFID_WRITER_ERROR, Toast.LENGTH_SHORT).show();
			writerState = false;
			return false;
		}
	}

	/**
	 * Barcode call back
	 */
	public Barcode2DWithSoft.ScanCallback mScanCallback = new Barcode2DWithSoft.ScanCallback() {

		@Override
		public void onScanComplete(int symbology, int length, byte[] data) {
			Tracer.debug(TAG, "[onScanComplete] _ " + "");
			if (length > 1) {
				String barCode = new String(data, 0, length);
				bListener.onBarCodeResponse(barCode);

			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		Tracer.debug(TAG, "[onResume] _ " + "");
		if (mReader == null) {
			instantiateReader();
		}
		if (mReader != null)
			mReader.setScanCallback(mScanCallback);
		if (mWriteRFID != null && writerState)
			new InitTask().execute();
		else
			instantiateRFIDWriter();

	}

	/**
	 * Thread to scan seals
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	private class DecodeThread extends Thread {
		private int sleepTime = 1000;

		public DecodeThread(BarCodeListener barCodeListener) {
			bListener = barCodeListener;
		}

		@Override
		public void run() {
			super.run();
			Tracer.debug(TAG, "[run] _ " + "");
			do {
				mReader.scan();

				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} while (!readerThreadStop);

		}
	}





	/**
	 * Write to tag.
	 * @param tagValue
	 */
	private void writetoTag(String tagValue)
	{
		Tracer.debug(TAG, "[writetoTag] _ " + "");
		String rfidWriteStatus = "";
		String accessPassword = "00000000";
		String text_append_tring = "3000";
		String bank = "UII";
		int ptr = 2;
		int cnt = 6;
		rfidWriteStatus = mWriteRFID.writeData(accessPassword, BankEnum.valueOf(bank), ptr, cnt, currentSealWriteValue);
		Tracer.debug(TAG, "[writetoTag] _ " + rfidWriteStatus);
		Tracer.debug(TAG, "[writetoTag] _ " + currentSealWriteValue);
		if(rfidWriteStatus != null && rfidWriteStatus.equalsIgnoreCase(text_append_tring + currentSealWriteValue))
		{
			Tracer.debug(TAG, "[writetoTag] _ " + "if case");
			WriteStatus mWriteStatus = new WriteStatus();
			mWriteStatus.setStatus(rfidWriteStatus);
			Message mWrite = new Message();
			mWrite.obj = mWriteStatus;
			mWriteHandler.sendMessage(mWrite);
			run = false;
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		Tracer.debug(TAG, "[onPause] _ " + "");
		readerThreadStop = true;
		if (mReader != null) {
			mReader.stopScan();
			mReader.close();
			mReader = null;
		}
		if (mWriteRFID != null) {
			mWriteRFID.free();
		}
	}

	/**
	 * Bar code scan
	 * 
	 * @param barCodeListener
	 */
	protected void startBarCodeScan(BarCodeListener barCodeListener) {
		Tracer.debug(TAG, "[startBarCodeScan] _ " + "");
		if (readerState)
			new DecodeThread(barCodeListener).start();
	}

	/**
	 * Write hex seal to RFID Tag.
	 * 
	 * @param convertedHexSeal
	 * @param rListener
	 */
	protected void writetoRFIDTag(String convertedHexSeal, RFIDSuccessUpdateListener rListener) {
		Tracer.debug(TAG, "[writetoRFIDTag] _ " + "");
		rfidSuccessListener = rListener;
		if (writerState && run)
		{
			currentSealWriteValue = convertedHexSeal;
			writerState = false;
			cTimer= new CountDownTimer(6000, 200) {

				@Override
				public void onTick(long millisUntilFinished) {
					Tracer.debug(TAG, "[onTick] _ " + "");
					if(run)
					{
						writetoTag(currentSealWriteValue);
					}
				}

				@Override
				public void onFinish() {
					Tracer.debug(TAG, "[onFinish] _ " + "");
					if(run)
					{
						WriteStatus mWriteStatus = new WriteStatus();
						mWriteStatus.setStatus(BaseActivity.RFID_TAG_WRITE_ERROR);
						Message mWrite = new Message();
						mWrite.obj = mWriteStatus;
						mWriteHandler.sendMessage(mWrite);
						if(cTimer != null)
						{
							cTimer.cancel();
							cTimer = null;
						}
						run = true;
						writerState = true;
					}
				}
			}.start();

		}
		else
		{
			if(!writerState)
				instantiateRFIDWriter();
		}

	}

	/**
	 * write status of the tag
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	public class WriteStatus {
		private String status;

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

	}

	/**
	 * Writer handler to handle callbacks from thread regarding status.
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	protected static class WriterHandler extends Handler {

		Context mContext;

		public WriterHandler(Context activityContext) {
			Tracer.debug(TAG, "[WriterHandler] _ " + "");
			mContext = activityContext;
		}

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try {
				Tracer.error(TAG, "[handleMessage] _ " + (msg != null) + "      " + (msg != null && msg.obj != null) + "    " + (msg != null && msg.obj != null && msg.obj instanceof WriteStatus));

				if (msg != null && msg.obj != null && msg.obj instanceof WriteStatus) {
					if(cTimer != null) {
						cTimer.cancel();
						cTimer = null;
						writerState = true;
						run = true;
					}
					WriteStatus mWriteStatus = (WriteStatus) msg.obj;
					Tracer.debug(TAG, "[handleMessage] _ " + "value of written sseal :- " + mWriteStatus.getStatus());
					rfidSuccessListener.onRFIDSuccessUpdate(mWriteStatus.getStatus());

				}
			} catch (Exception e) {
				Tracer.debug(TAG, "[handleMessage] _ " + "Catch exception:" + e.toString());
				e.printStackTrace();
			}
		}
	}

	/**
	 * get activity context
	 * 
	 * @return
	 */
	private Context getActivityContext() {
		return BaseActivity.this;
	}

	/**
	 * convert to hex code of seal for rfid writing
	 * 
	 * @param result
	 */
	protected String convert2HexSeals(String result) {
		Tracer.debug(TAG, "[convert2HexSeals] _ " + "");
		StringBuffer hexString = new StringBuffer();
		if (UtilsDelhivery.checkNotNullEmpty(result)) {
			Tracer.debug(TAG, "[convert2Hex] _ " + "");
			String diffValues = "";
			hexString.append(asciiToHex(result));
			int lenHexString = hexString.length();
			Tracer.debug(TAG, "[convert2HexSeals] _ " + "length" + lenHexString);
			int difference = RFID_TAG_SIZE - lenHexString;
			Tracer.debug(TAG, "[convert2HexSeals] _ " + "difference" + difference);
			if (difference < 0)
				hexString.append(INVALID_STRING);
			else {

				for (int i = 0; i < difference; i++)
					diffValues = diffValues.concat("f");
			}
			hexString.append(diffValues);
			Tracer.debug(TAG, "[convert2HexSeals] _ " + hexString.toString());
		}

		return hexString.toString();
	}

	/**
	 * ascii to hex conversion
	 * 
	 * @param expression
	 */
	private String asciiToHex(String expression) {
		Tracer.debug(TAG, "[asciiToHex] _ " + "");
		char[] chars = expression.toCharArray();
		String null_seal_string = "ff";
		StringBuffer temp_str = new StringBuffer();
		StringBuffer sBuffer = new StringBuffer();

		for (int i = 0; i <= 3; i++) {
			if (Character.isLetter(chars[i]) && i <= 3) {
				sBuffer.append(Integer.toHexString((int) chars[i]));
			} else {
				sBuffer.append(null_seal_string);
				temp_str.append(chars[i]);
			}
		}
		if (temp_str != null) {
			sBuffer.append(temp_str);
		}
		for (int i = 4; i < chars.length; i++) {
			if (Character.isLetter(chars[i])) {
				sBuffer.append(Integer.toHexString((int) chars[i]));
			} else
				sBuffer.append(chars[i]);
		}
		return sBuffer.toString();
	}
}
