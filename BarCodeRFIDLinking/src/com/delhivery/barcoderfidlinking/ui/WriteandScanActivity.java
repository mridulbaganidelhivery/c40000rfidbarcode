package com.delhivery.barcoderfidlinking.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.delhivery.barcoderfidlinking.listener.BarCodeListener;
import com.delhivery.barcoderfidlinking.listener.RFIDSuccessUpdateListener;
import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.Tracer;

public class WriteandScanActivity extends BaseActivity implements OnEditorActionListener{

	private final String TAG = AppConfig.BASE_TAG
			+ WriteandScanActivity.class.getSimpleName();
	private EditText manualNumberEt;
	private LinearLayout manualNumberLl;
	private String lastScanId;
	private TextView tLastScanValue;
	private TextView tRFIDTagValue;
	private TextView tCount;
	private int HARD_KEY_VALUE = 139;
	private String lastSeal ="";
	private static int countItem;
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Tracer.debug(TAG, "[onCreate] _ " + "");
		setContentView(R.layout.activity_write_tag);
		initRFIDBeepSound();
		countItem = 0;
		setViews();
	}

	/**
	 * Initializes Views and sets Click Listeners
	 */
	private void setViews() {
		Tracer.debug(TAG, "[setViews] _ " + "");
		manualNumberLl = (LinearLayout) findViewById(R.id.barcode_manual_id_ll);
		tCount = (TextView) findViewById(R.id.item_count_value);
		tLastScanValue = (TextView) findViewById(R.id.itemseal_value);
		tRFIDTagValue = (TextView) findViewById(R.id.itemrfid_value);
		manualNumberEt = (EditText) findViewById(R.id.barcode_manual_item_et);		
		manualNumberEt.setOnEditorActionListener(this);
		showManualScan(true);
		manualNumberEt.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				Tracer.debug(TAG, "[onKey] _ " + "");
				if ((keyCode == HARD_KEY_VALUE
						&& event.getAction() == KeyEvent.ACTION_DOWN) || (keyCode == KeyEvent.KEYCODE_ENTER 
								&& event.getAction() == KeyEvent.ACTION_DOWN)) {

					Tracer.debug(TAG, "[onKey] _ " + "" + keyCode);
					startBarCodeScan(barCodeListener);
					return true;
				}
				return false;
			}
		});
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		switch (v.getId()) {
		case R.id.barcode_manual_item_et:
			String convertedHexSeal = convert2HexSeals(manualNumberEt.getText().toString());
			if(convertedHexSeal.matches("[f0-9]+") && convertedHexSeal.length() > 2)
			{
				Tracer.debug(TAG, "[onEditorAction] _ " + "if");
				writetoRFIDTag(convertedHexSeal,rfidUpdateListener);
			}
			else
			{
				Tracer.debug(TAG, "[onEditorAction] _ " + "else");
				showManualScan(true);
				Toast.makeText(getActivityContext(), convertedHexSeal, Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}
		return false;
	}


	/**
	 * Show {@link EditText} for manual entry for bar code entry
	 * 
	 * @param show
	 */
	private void showManualScan(boolean show) {
		Tracer.debug(TAG, "[showManualScan] _ " + show);
		if (show) {
			manualNumberLl.setVisibility(View.VISIBLE);
			if (lastScanId != null)
				manualNumberEt.setHint(lastScanId);
			else
				manualNumberEt.setHint("Scan");

		} else {
			manualNumberLl.setVisibility(View.GONE);
			if (lastScanId == null) {
				manualNumberEt.setHint("Scan");
			} else
				manualNumberEt.setHint(lastScanId);
		}
	}

	BarCodeListener barCodeListener = new BarCodeListener() {

		@Override
		public void onBarCodeResponse(String result) {
			Tracer.debug(TAG, "[onBarCodeResponse] _ " + "result:" + result);
			lastSeal = result;
			String convertedHexSeal = convert2HexSeals(result);
			if(convertedHexSeal.matches("[fca0-9]+") && convertedHexSeal.length() > 2)
			{
				Tracer.debug(TAG, "[onBarCodeResponse] _ " + "conversion:" + convertedHexSeal);
				manualNumberEt.setHint(result);
				writetoRFIDTag(convertedHexSeal,rfidUpdateListener);
			}
			else
			{
				Tracer.debug(TAG, "[onBarCodeResponse] _ " + "conversion:" + convertedHexSeal);
				Toast.makeText(getActivityContext(), convertedHexSeal, Toast.LENGTH_SHORT).show();
			}

		}

	};


	/**
	 * get activity context
	 * @return
	 */
	private Context getActivityContext()
	{
		return WriteandScanActivity.this;
	}

	/**
	 * set last successful written value on RFID tag.
	 * @param lastScanValue
	 */
	private void setLastScanned(String lastScanValue)
	{
		Tracer.debug(TAG, "[setLastScanned] _ " + "");
		if(lastScanValue.equalsIgnoreCase(""))
		{
			lastSeal = lastScanValue;
			manualNumberEt.setHint("Scan");
			tLastScanValue.setText("");
			tRFIDTagValue.setText("");
		}
		else
		{
			playRFIDBeepSound();
			tLastScanValue.setText(lastSeal);
			manualNumberEt.setHint(lastSeal);
			tRFIDTagValue.setText(lastScanValue);
			countItem +=1;
			tCount.setText("" + countItem);	
		}

	}

	RFIDSuccessUpdateListener rfidUpdateListener = new RFIDSuccessUpdateListener() {

		@Override
		public void onRFIDSuccessUpdate(String updatedValue) {
			Tracer.debug(TAG, "[onRFIDSuccessUpdate] _ " + "value :-" + updatedValue);
			if(updatedValue.equalsIgnoreCase(BaseActivity.RFID_TAG_WRITE_ERROR))
			{
				Toast.makeText(getActivityContext(), BaseActivity.RFID_TAG_WRITE_ERROR, Toast.LENGTH_SHORT).show();
			}
			else
			{
				setLastScanned(updatedValue);	
			}
			
		}
	};

}
