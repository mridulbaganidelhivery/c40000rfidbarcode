package com.delhivery.barcoderfidlinking.utils;

import android.util.Log;

/**
 * The Class Tracer. Wrapper class for {@link Log}
 * 
 * @author Rushil
 */
public class Tracer {

	/** The Constant LOG_ENABLE. */
	private static boolean logEnable = true;

	public static boolean isLogEnable() {
		return logEnable;
	}

	public static void setLogEnable(boolean logEnable) {
		Tracer.logEnable = logEnable;
	}

	/**
	 * {@link Log} Information.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void debug(String TAG, String message) {
		if (logEnable) {
			Log.i(TAG, message);
		}
	}

	/**
	 * {@link Log} Error.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void error(String TAG, String message) {
		if (logEnable) {
			Log.e(TAG, message);
		}
	}

	/**
	 * {@link Log} Debug.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void d(String TAG, String message) {
		if (logEnable) {
			Log.d(TAG, message);
		}
	}

	/**
	 * {@link Log} important.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void imp(String TAG, String message) {
		if (logEnable) {
			Log.i(TAG + "_imp", message);
		}
	}

	/**
	 * Info.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void in(String TAG, String message) {

		Log.i(TAG + "_info", message);

	}

	/**
	 * Error.
	 * 
	 * @param TAG
	 *            the tag
	 * @param message
	 *            the message
	 */
	public static void er(String TAG, String message) {

		Log.e(TAG + "_error", message);

	}

}
