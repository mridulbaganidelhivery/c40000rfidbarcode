package com.delhivery.barcoderfidlinking.utils;

import java.util.HashSet;

/**
 * Bag seal correction
 * 
 * @author Mridul Bagani
 * 
 */
public class BagSealCorrect {

	private static final String TAG = AppConfig.BASE_TAG
			+ BagSealCorrect.class.getSimpleName();
	private static HashSet<String> validIdentifiers;
	static {
		validIdentifiers = new HashSet<String>();
		validIdentifiers.add("424147");
		validIdentifiers.add("4241474A");
		validIdentifiers.add("4241474C");
	}
	private static final String BAG_PREFIX = "424147";
	private static final String BAG_PREFIX_LARGE = "4241474C";
	private static final String BAG_PREFIX_JUMBO = "4241474A";

	public static String RFIDTagConversion(String tag) {
		Tracer.debug(TAG, "[BagSealCorrection] _ " + "");
		tag = tag.replaceAll("F", "").trim();
		
		if(tag.startsWith(BAG_PREFIX_JUMBO)){
			return tag.replace(BAG_PREFIX_JUMBO, "BAGJ");
		}else if(tag.startsWith(BAG_PREFIX_LARGE)){
			return tag.replace(BAG_PREFIX_LARGE, "BAGL");
		}else if(tag.startsWith(BAG_PREFIX)){
			return tag.replace(BAG_PREFIX, "BAG");
		}else{
			return null;
		}
	}
}