package com.delhivery.barcoderfidlinking.utils;

/**
 * App configurations
 * @author Mridul Bagani
 *
 */
public class AppConfig {
	
	public static final String BASE_TAG = "delhivery_barcoderfid";
	
	public static final String CrittercismId = "939661f5329d4e398d129d514d3cf62300555300";

}
