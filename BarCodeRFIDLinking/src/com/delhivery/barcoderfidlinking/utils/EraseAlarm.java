package com.delhivery.barcoderfidlinking.utils;

import java.util.Calendar;

import com.delhivery.barcoderfidlinking.service.EraseTagService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
/**
 * Alarm service for erarsing tag.
 * @author Mridul Bagani
 *
 */
public class EraseAlarm {

	private final String TAG = AppConfig.BASE_TAG + EraseAlarm.class.getSimpleName();
	private AlarmManager alarmManager;
	private Intent intent;
	private PendingIntent pendingIntent;
	private Calendar cal;

	public EraseAlarm(Context context) {
		Tracer.debug(TAG, "[EraseAlarm] _ " + "");
		cal = Calendar.getInstance();
		intent = new Intent(context, EraseTagService.class);
		alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

	}

	public void startAlaram(long delay) {
		Tracer.debug(TAG, "[startAlaram] _ " + "");
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), delay,
				pendingIntent);
	}

	public void reschedule(long delay) {
		Tracer.debug(TAG, "[reschedule] _ " + "");
		alarmManager.cancel(pendingIntent);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), delay,
				pendingIntent);
	}

	public void cancel() {
		Tracer.debug(TAG, "[cancel] _ " + "");
		alarmManager.cancel(pendingIntent);
	}

}
