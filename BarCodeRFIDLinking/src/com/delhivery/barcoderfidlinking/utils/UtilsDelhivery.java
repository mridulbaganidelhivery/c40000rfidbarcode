package com.delhivery.barcoderfidlinking.utils;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Typeface;

/**
 * 
 * @author Mridul Bagani
 *
 */
public class UtilsDelhivery {

	private static String TAG = AppConfig.BASE_TAG + UtilsDelhivery.class.getSimpleName();
	
	/**
	 * Checks if the given {@link String} is empty or not.
	 * 
	 * @param str
	 * @return isNull
	 */
	public static boolean checkNotNullEmpty(String str) {
		Tracer.debug(TAG, "[checkNotNullEmpty] _ " + "");
		if (str == null || str.equals("") || str.equals("null")) {
			return false;
		} else {
			return true;
		}
	}
	
	private static HashMap<String, Typeface> typefaces = new HashMap<String, Typeface>();
	
	
	/**
	 * Sets {@link Typeface} with respect to {@link Context}
	 * 
	 * @param ctx
	 * @param typefaceName
	 * @return
	 */
	public static Typeface getTypeface(Context ctx, String typefaceName) {
		if (!typefaces.containsKey(typefaceName)) {
			Typeface tempTypeface = Typeface.createFromAsset(ctx.getAssets(),
					typefaceName + ".ttf");
			typefaces.put(typefaceName, tempTypeface);
		}
		return typefaces.get(typefaceName);

	}
	
	
}
