package com.delhivery.barcoderfidlinking.utils;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.delhivery.barcoderfidlinking.service.SearchTagService;
/**
 * Alarm service for erarsing tag.
 * @author Mridul Bagani
 *
 */
public class SearchAlarm {

	private final String TAG = AppConfig.BASE_TAG + EraseAlarm.class.getSimpleName();
	private AlarmManager alarmManager;
	private Intent intent;
	private PendingIntent pendingIntent;
	private Calendar cal;

	public SearchAlarm(Context context) {
		Tracer.debug(TAG, "[SearchAlarm] _ " + "");
		cal = Calendar.getInstance();
		intent = new Intent(context, SearchTagService.class);
		alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

	}

	public void startAlaram(long delay) {
		Tracer.debug(TAG, "[startAlaram] _ " + "");
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), delay,
				pendingIntent);
	}

	public void reschedule(long delay) {
		Tracer.debug(TAG, "[reschedule] _ " + "");
		alarmManager.cancel(pendingIntent);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), delay,
				pendingIntent);
	}

	public void cancel() {
		Tracer.debug(TAG, "[cancel] _ " + "");
		alarmManager.cancel(pendingIntent);
	}

}
