package com.delhivery.barcoderfidlinking.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.Tracer;
import com.delhivery.barcoderfidlinking.utils.UtilsDelhivery;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.RFIDWithUHF.BankEnum;
import com.rscja.deviceapi.entity.SimpleRFIDEntity;

/**
 * Erase service for the tags.
 * 
 * @author Mridul Bagani
 * 
 */
public class EraseTagService extends Service {

	private final String TAG = AppConfig.BASE_TAG + EraseTagService.class.getSimpleName();
	private int mStartMode;
	private IBinder mBinder = new MyBinder();
	private static RFIDWithUHF mWriteRFID;
	private boolean writerState = true;
	private SimpleRFIDEntity erasedTagValueEntity;
	private String lastScanned;
	private static final String eraseValue = "ffffffffffffffffffffffff";
	private String ERASE_TAG = "erase_tag";

	@Override
	public void onCreate() {
		super.onCreate();
		Tracer.debug(TAG, "[onCreate] _ " + "");
		instantiateRFIDEraser();
	}


	/**
	 * instantiate rfid eraser
	 * 
	 * @return
	 */
	private Boolean instantiateRFIDEraser() {
		Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "");
		try {
			mWriteRFID = RFIDWithUHF.getInstance();
			writerState = true;
			if (mWriteRFID != null && writerState)
				new InitTask().execute();
			return true;

		} catch (Exception ex) {
			Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "catch" + ex.toString());
			Toast.makeText(this, "RFID Writer not working.Scan again!", Toast.LENGTH_SHORT).show();
			writerState = false;
			return false;
		}
	}

	/**
	 * init writer RFID.
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	public class InitTask extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			Tracer.debug(TAG, "[doInBackground] _ " + "");
			return mWriteRFID.init();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Tracer.debug(TAG, "[onPostExecute] _ " + "");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Tracer.debug(TAG, "[onPreExecute] _ " + "");
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		Tracer.debug(TAG, "[onBind] _ " + "");
		return mBinder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Tracer.debug(TAG, "[onStartCommand] _ " + "");
		eraseRFIDTag();
		return mStartMode;
	}

	public void eraseRFIDTag() {
		Tracer.debug(TAG, "[eraseRFIDTag] _ " + "");
		String accessPassword = "00000000";
		String bank = "UII";
		int ptr = 2;
		int cnt = 6;
		erasedTagValueEntity = mWriteRFID.readData(accessPassword, BankEnum.valueOf(bank), ptr, cnt);
		if(erasedTagValueEntity != null)
		{
			lastScanned = erasedTagValueEntity.getData();
			Tracer.debug(TAG, "[eraseRFIDTag] _ " + "oldvalue" + lastScanned);
		}
		else
		{
			lastScanned = "";
			Tracer.debug(TAG, "[eraseRFIDTag] _ " + "oldvalue" + "");
		}
		if(!lastScanned.equalsIgnoreCase(eraseValue))
		{
			String rfidWriteStatus = mWriteRFID.writeData(accessPassword, BankEnum.valueOf(bank), ptr, cnt, eraseValue);
			Tracer.debug(TAG, "[eraseRFIDTag] _ " + "newValue" + rfidWriteStatus);
			if(UtilsDelhivery.checkNotNullEmpty(rfidWriteStatus) && lastScanned != null)
			{
				//				String trim_sealValue = rfidWriteStatus.substring(4);
				Intent iData = new Intent(ERASE_TAG);
				iData.putExtra("oldValue", lastScanned);
				iData.putExtra("newValue", eraseValue);
				LocalBroadcastManager.getInstance(this).sendBroadcast(iData);
			}
			else
			{
				Intent iData = new Intent(ERASE_TAG);
				iData.putExtra("oldValue", "");
				iData.putExtra("newValue", "");
				LocalBroadcastManager.getInstance(this).sendBroadcast(iData);
			}		
		}
		else
		{
			//if already deleted and done.
			Intent iData = new Intent(ERASE_TAG);
			iData.putExtra("oldValue", "Already erased!Use another tag.");
			iData.putExtra("newValue", "Already erased!Use another tag.");
			LocalBroadcastManager.getInstance(this).sendBroadcast(iData);
		}

	}

	@Override
	public void onRebind(Intent intent) {
		Tracer.debug(TAG, "[onRebind] _ " + "");
		super.onRebind(intent);
		if (mWriteRFID != null && writerState)
			new InitTask().execute();
		else
			instantiateRFIDEraser();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Tracer.debug(TAG, "[onUnbind] _ " + "");
		if (mWriteRFID != null) {
			mWriteRFID.free();
		}
		return true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Tracer.debug(TAG, "[onDestroy] _ " + "");
	}


	public class MyBinder extends Binder {
		public EraseTagService getService() {
			Tracer.debug(TAG, "[getService] _ " + "");
			return EraseTagService.this;
		}

	}

}
