package com.delhivery.barcoderfidlinking.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.Tracer;
import com.rscja.deviceapi.RFIDWithUHF;
import com.rscja.deviceapi.RFIDWithUHF.BankEnum;
import com.rscja.deviceapi.entity.SimpleRFIDEntity;

/**
 * Search service for the tags.
 * 
 * @author Mridul Bagani
 * 
 */
public class SearchTagService extends Service {

	private final String TAG = AppConfig.BASE_TAG + SearchTagService.class.getSimpleName();
	private int mStartMode;
	private IBinder mBinder = new MyBinder();
	private static RFIDWithUHF mSearchRFID;
	private boolean searchState = true;
	private SimpleRFIDEntity searchTagValueEntity;
	private String lastScanned;
	private String SEARCH_TAG = "search_tag";

	@Override
	public void onCreate() {
		super.onCreate();
		Tracer.debug(TAG, "[onCreate] _ " + "");
		instantiateRFIDEraser();
	}


	/**
	 * instantiate rfid eraser
	 * 
	 * @return
	 */
	private Boolean instantiateRFIDEraser() {
		Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "");
		try {
			mSearchRFID = RFIDWithUHF.getInstance();
			searchState = true;
			if (mSearchRFID != null && searchState)
				new InitTask().execute();
			return true;

		} catch (Exception ex) {
			Tracer.debug(TAG, "[instantiateRFIDWriter] _ " + "catch" + ex.toString());
			Toast.makeText(this, "RFID Writer not working.Scan again!", Toast.LENGTH_SHORT).show();
			searchState = false;
			return false;
		}
	}

	/**
	 * init writer RFID.
	 * 
	 * @author Mridul Bagani
	 * 
	 */
	public class InitTask extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			Tracer.debug(TAG, "[doInBackground] _ " + "");
			return mSearchRFID.init();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Tracer.debug(TAG, "[onPostExecute] _ " + "");
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Tracer.debug(TAG, "[onPreExecute] _ " + "");
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		Tracer.debug(TAG, "[onBind] _ " + "");
		return mBinder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Tracer.debug(TAG, "[onStartCommand] _ " + "");
		searchTag();
		return mStartMode;
	}

	public void searchTag() {
		Tracer.debug(TAG, "[searchTag] _ " + "");
		String accessPassword = "00000000";
		String bank = "UII";
		int ptr = 2;
		int cnt = 6;
		searchTagValueEntity = mSearchRFID.readData(accessPassword, BankEnum.valueOf(bank), ptr, cnt);
		if(searchTagValueEntity != null)
		{
			Tracer.debug(TAG, "[searchTag] _ " + lastScanned);
			lastScanned = searchTagValueEntity.getData();
			Intent iData = new Intent(SEARCH_TAG);
			iData.putExtra("searchTag", lastScanned);
			LocalBroadcastManager.getInstance(this).sendBroadcast(iData);
		}
		else
		{
			lastScanned = "No Tag Found!";
			Intent iData = new Intent(SEARCH_TAG);
			iData.putExtra("searchTag", lastScanned);
			LocalBroadcastManager.getInstance(this).sendBroadcast(iData);
		}
	}

	@Override
	public void onRebind(Intent intent) {
		Tracer.debug(TAG, "[onRebind] _ " + "");
		super.onRebind(intent);
		if (mSearchRFID != null && searchState)
			new InitTask().execute();
		else
			instantiateRFIDEraser();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Tracer.debug(TAG, "[onUnbind] _ " + "");
		if (mSearchRFID != null) {
			mSearchRFID.free();
		}
		return true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Tracer.debug(TAG, "[onDestroy] _ " + "");
	}


	public class MyBinder extends Binder {
		public SearchTagService getService() {
			Tracer.debug(TAG, "[getService] _ " + "");
			return SearchTagService.this;
		}

	}

}
