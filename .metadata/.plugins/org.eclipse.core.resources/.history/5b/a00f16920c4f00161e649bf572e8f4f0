package com.delhivery.barcoderfidlinking.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.TextView;

import com.delhivery.barcoderfidlinking.service.SearchTagService;
import com.delhivery.barcoderfidlinking.service.SearchTagService.MySearchBinder;
import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.BagSealCorrect;
import com.delhivery.barcoderfidlinking.utils.SearchAlarm;
import com.delhivery.barcoderfidlinking.utils.Tracer;

/**
 * erase all the tags
 * 
 * @author Mridul Bagani
 * 
 */
public class SearchActivity extends BaseActivity {

	private final String TAG = AppConfig.BASE_TAG + EraseActivity.class.getSimpleName();
	private TextView tScanCount;
	private TextView tNewValue;
	private SearchTagService mBoundService;
	private boolean mServiceBound = false;
	private static int countItem;
	private String SEARCH_TAG = "search_tag";
	private SearchAlarm eSearchAlarm;
	private static final String NO_TAG_FOUND = "No Tag Found!";
	private static final String NULL_VALUE = "Null Value!";
	private static final String nulltagValue = "FFFFFFFFFFFFFFFFFFFFFFFF";
	private static final String INVALID_TAG = "Invalid Tag Value";
	private static final String EMPTY_TAG = "Empty Tag";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		Tracer.debug(TAG, "[onCreate] _ " + "");
		countItem = 0;
		setViews();
	}


	@Override
	protected void onStart() {
		super.onStart();
		Tracer.debug(TAG, "[onStart] _ " + "");
		LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
				new IntentFilter(SEARCH_TAG));
		Intent intent = new Intent(this, SearchTagService.class);
		startService(intent);
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onResume() {
		Tracer.debug(TAG, "[onResume] _ " + "");
		eSearchAlarm.startAlaram(2000);
		super.onResume();
	}

	/**
	 * Initializes Views and sets Click Listeners
	 */
	private void setViews() {
		Tracer.debug(TAG, "[setViews] _ " + "");
		tScanCount = (TextView) findViewById(R.id.item_count_value);
		tNewValue = (TextView)findViewById(R.id.itemrfid_value);
		eSearchAlarm = new SearchAlarm(getActivityContext());
	}

	@Override
	protected void onStop() {
		super.onStop();
		Tracer.debug(TAG, "[onStop] _ " + "");
		if (mServiceBound) {
			unbindService(mServiceConnection);
			mServiceBound = false;
		}
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		Intent intent = new Intent(SearchActivity.this, SearchTagService.class);
		stopService(intent);
		eSearchAlarm.cancel();
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Tracer.debug(TAG, "[onReceive] _ " + "");
			if(intent.getAction().equalsIgnoreCase(SEARCH_TAG))
			{
				updateData(intent.getStringExtra("searchTag"));
			}
		}
	};


	/**
	 * update data with old values and new values
	 * @param oldValue
	 * @param newValue
	 */
	private void updateData(String searchedTag) {
		Tracer.debug(TAG, "[updateData] _ " + "");
		if(searchedTag.equalsIgnoreCase(NO_TAG_FOUND))
		{
			tNewValue.setText(NO_TAG_FOUND);	
		}
		else if(searchedTag.equalsIgnoreCase(nulltagValue))
		{
			tNewValue.setText(NULL_VALUE);
		}
		else 
		{
			if(BagSealCorrect.RFIDTagConversion(searchedTag) != null)
			{
				tNewValue.setText(BagSealCorrect.RFIDTagConversion(searchedTag));
				countItem +=1;
				tScanCount.setText("" + countItem);				
			}
			else
			{
				if(searchedTag.length() > 0)
					tNewValue.setText(INVALID_TAG);
				else
					tNewValue.setText(EMPTY_TAG);
			}


		}

	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mServiceBound = false;
			Tracer.debug(TAG, "[onServiceDisconnected] _ " + "");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MySearchBinder myBinder = (MySearchBinder) service;
			mBoundService = myBinder.getService();
			Tracer.error(TAG, "[enclosing_method] _ " + (mBoundService != null));
			mServiceBound = true;
			Tracer.debug(TAG, "[onServiceConnected] _ " + "");
		}
	};

	protected void onDestroy() {
		Tracer.debug(TAG, "[onDestroy] _ " + "");
		super.onDestroy();

	}

	/**
	 * get activity context
	 * @return
	 */
	private Context getActivityContext()
	{
		return SearchActivity.this;
	}

}
