package com.delhivery.barcoderfidlinking.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.TextView;

import com.delhivery.barcoderfidlinking.service.EraseTagService;
import com.delhivery.barcoderfidlinking.service.EraseTagService.MyBinder;
import com.delhivery.barcoderfidlinking.utils.AppConfig;
import com.delhivery.barcoderfidlinking.utils.EraseAlarm;
import com.delhivery.barcoderfidlinking.utils.Tracer;

/**
 * erase all the tags
 * 
 * @author Mridul Bagani
 * 
 */
public class SearchActivity extends BaseActivity {

	private final String TAG = AppConfig.BASE_TAG + EraseActivity.class.getSimpleName();
	private TextView tScanValue;
	private TextView tScanCount;
	private TextView tNewValue;
	private EraseTagService mBoundService;
	private boolean mServiceBound = false;
	private static int countItem;
	private String ERASE_TAG = "erase_tag";
	private EraseAlarm eAlarm;
	private static final String eraseval = "ffffffffffffffffffffffff";
	private static final String eraseupdatedval = "FFFFFFFFFFFFFFFFFFFFFFFF";
	private static final String already_erased = "Already erased!Use another tag.";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_erase_tag);
		Tracer.debug(TAG, "[onCreate] _ " + "");
		countItem = 0;
		setViews();
	}


	@Override
	protected void onStart() {
		super.onStart();
		Tracer.debug(TAG, "[onStart] _ " + "");
		LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
				new IntentFilter(ERASE_TAG));
		Intent intent = new Intent(this, EraseTagService.class);
		startService(intent);
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	protected void onResume() {
		Tracer.debug(TAG, "[onResume] _ " + "");
		eAlarm.startAlaram(2000);
		super.onResume();
	}

	/**
	 * Initializes Views and sets Click Listeners
	 */
	private void setViews() {
		Tracer.debug(TAG, "[setViews] _ " + "");
		tScanValue = (TextView) findViewById(R.id.itemseal_value);
		tScanCount = (TextView) findViewById(R.id.item_count_value);
		tNewValue = (TextView)findViewById(R.id.itemrfid_value);
		eAlarm = new EraseAlarm(getActivityContext());
	}

	@Override
	protected void onStop() {
		super.onStop();
		Tracer.debug(TAG, "[onStop] _ " + "");
		if (mServiceBound) {
			unbindService(mServiceConnection);
			mServiceBound = false;
		}
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		Intent intent = new Intent(SearchActivity.this, EraseTagService.class);
		stopService(intent);
		eAlarm.cancel();
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Tracer.debug(TAG, "[onReceive] _ " + "");
			if(intent.getAction().equalsIgnoreCase(ERASE_TAG))
			{
				updateData(intent.getStringExtra("oldValue"),intent.getStringExtra("newValue"));
			}
		}
	};


	/**
	 * update data with old values and new values
	 * @param oldValue
	 * @param newValue
	 */
	private void updateData(String oldValue, String newValue) {
		Tracer.debug(TAG, "[updateData] _ " + "");
		if(newValue.equalsIgnoreCase(eraseval))
		{
			tNewValue.setText(eraseupdatedval);	
		}
		else
		{
			tNewValue.setText(newValue);
		}
		tScanValue.setText(oldValue);
		if(newValue.length() != 0 && !newValue.equalsIgnoreCase(already_erased))
		{
			countItem +=1;
			tScanCount.setText("" + countItem);							
		}

	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mServiceBound = false;
			Tracer.debug(TAG, "[onServiceDisconnected] _ " + "");
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			MyBinder myBinder = (MyBinder) service;
			mBoundService = myBinder.getService();
			Tracer.error(TAG, "[enclosing_method] _ " + (mBoundService != null));
			mServiceBound = true;
			Tracer.debug(TAG, "[onServiceConnected] _ " + "");
		}
	};

	protected void onDestroy() {
		Tracer.debug(TAG, "[onDestroy] _ " + "");
		super.onDestroy();

	}

	/**
	 * get activity context
	 * @return
	 */
	private Context getActivityContext()
	{
		return SearchActivity.this;
	}

}
